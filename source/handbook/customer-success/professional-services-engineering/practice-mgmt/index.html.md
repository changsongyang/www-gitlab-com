---
layout: handbook-page-toc
title: "PS Practice Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Here are workflows for the main practice management areas of responsibility.

### Creating a new Professional Services SKU

SKUs for new PS offerings are requested by the PS Practice Manager. To request a new SKU, the Practice Manager must determine and complete the following items and steps to gain approval.

#### Required items

* A specimen SOW that can be referenced by the Item
* Established cost-basis and assumptions
* Hours and hourly cost
* Delivery cost (hours x hourly cost)
* Included T&E assumption (if any)

#### Steps for creating a SKU

The Practice Manager follows these steps to request creation of a new SKU.

1. Create an [issue in the Finance issue tracker](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=New_PS_SKU) referencing the above requirements
1. Review with the Finance Business Partner for Sales
1. Make any require changes
1. Submit to VP of Customer Success for approval
1. Submit to CFO, CRO and CEO for approval
1. Once approved, submit to Accounting to create SKU in Zuora (note: Accounting does not create a SKU number but does add the offering to the product catalog)
1. Once it is added to the product catalog, make up a new SKU number for the offering and add it to the Current SKUs listing by editing the services.yaml file and creating a merge request.

### Workflow Labels

Here are the main labels used for PS practice management projects.

#### Practice-agnostic labels

`Practice Management` - Label for improvements to existing services or adding new services

#### Education Services practice management Labels

Here are the main labels used for Education Practice Management projects.

`ProServ-practice::Education`

`Workflow::validation backlog` - Team member identifies audience needs and potential education services offering

`Workflow::problem validation` - Practice manager validates needs addressed by proposed Education Services

`Workflow::design` - Practice manager defines solution component requirements and confirms with ID that the proposed solution is feasible 

`Workflow::solution validation` - Practice manager validates proposed Education Services solution will meet business objectives

`Workflow::planning breakdown` - Practice manager works with ID to plan project work with issues and epics

### Education Services go to market workflow for new offerings

#### Sales-team-assisted go to market motion

1. When new Education Services are launched they include internal communications and collateral to sales reps
    * Announcement of availability in public sector and sales slack channels
    * Include in Sale Enablement training
    * Offering description pages on /services
    * Success Stories collateral
    * Slides for sales decks
    * Inclusion of offering in [Services Calculator](https://services-calculator.gitlab.io/)
    * If applicable, Standard variant with:
        * Standard SOW
        * Finalized SKU for use by Sales
1. Rep advises customer on which Education Services are needed
1. Sales rep adds SKUs to customer contract
1. PS delivers Private Onsite or Remote ILTs
1. Education Services provisions account-level subscription access to LXP for all LXP subscription SKUs

#### Web-direct go to market motion 

1. When new Education Services are launched they include external communications to GitLab ecosystem as per the Marketing Strategy, including:
    * Nurture campaign emails
    * In-product banner
    * Recommended by SA or TAM
    * Included on Training Tracks page
    * Included on Education Services pages
    * Mentioned in product release updates
    * Highlighted in blog post
1. Individual users click Order button and pay using credit card from customers.gitlab.com and from within LXP
1. Users attend Public Remote ILTs via Zoom Webinar
1. Users access certification learning paths from LXP

### Marketing Strategy

* **Add to customers.gitlab.com ordering page**: Gives visibility and leads user down path of discovering more offerings
* **Event Presence**: Education Services presence at Commit
* **Community and Evangelism**: 
    * **GitLab Heros**: Pilot with GitLab Heros to test out certifications. Have Heros participate in training in advance of commit and then iterate based on that. Offer trainings for free and offer free exam to get gitlab certified ahead of time (John Coghlan is contact)
    * **GitLab Evangelism team**: Collaboration initiatives TBD
* **Add to Customer Success Plan**: Make certifications part of success plan from TAM. Set metrics against percentage of users who complete certifications
* Offerings integrated into customer adoption nurture campaign
* In-product awareness 
* Direct marketing on 3rd party platforms/communities where target audience lives
    * edEx, EdCast Leapest Marketplace, LinkedIn, Dice.com
    * Blog posts -- GitLab and 3rd party sites (eg StackOverflow, FB)

### How to edit a Professional Services product page

PS uses a file called services.yml as the single source of truth (SSoT) for offering descriptions and specifications. This file is used together with a documentation macro to populate various parts of the GitLab documentation and is also used by the [Services Calculator](https://services-calculator.gitlab.io/). The [Services SSOT Wiki Page](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-process/-/wikis/Services-Single-Source-of-Truth) describes its various applications, attributes and maintenance processes. Team members who wish to update or add offerings can follow the instructions located on the wiki page. 
