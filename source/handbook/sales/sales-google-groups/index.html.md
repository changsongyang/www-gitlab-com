---
layout: handbook-page-toc
title: "Sales Google Groups"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Google Groups

The parent google group of the sales team is sales-all. Several child groups fall within the parent group. Please see the below chart:

![Sales Google Groups](/handbook/sales/images/newsalesgg.jpg)
