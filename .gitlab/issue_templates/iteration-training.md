# Deprecated

This template has been deprecated.  The Iteration Training Template has been moved to the [Product Project](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/iteration-training.md)

To create a new Iteration Training Issue, please use [this](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training) link.
